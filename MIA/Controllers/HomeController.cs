﻿using MIA.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;


namespace MIA.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// index action result
        /// </summary>
        /// <returns>index view</returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult GetSiteLocation()
        {
            List<SiteLocation> sites = new List<SiteLocation>();
            List<getSiteLocations_Result> siteResults = HomeModel.GetSites();

            foreach (var siteResult in siteResults)
            {
                SiteLocation site = new SiteLocation();
                site.SiteScope = new SiteScope();

                site.Latitude = siteResult.latitude;
                site.Longitude = siteResult.longitude;
                site.ScopeColor = siteResult.color;
                site.ScopeIcon = siteResult.icon;
                site.SiteAddress1 = siteResult.address1;
                site.SiteAddress2 = siteResult.address2;
                site.SiteAddress3 = siteResult.address3;
                site.SiteCity = siteResult.city;
                site.SiteCountry = siteResult.region;
                site.SitePostcode = siteResult.postcode;
                site.SiteName = siteResult.name;

                site.SiteScope.ScopeId = siteResult.scopeid;

                sites.Add(site);
            }
            return Json(sites, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult GetScopeList()
        {
            List<SiteScope> scopes = new List<SiteScope>();
            List<getSiteLocationScope_Result> siteScopes = HomeModel.GetSiteScopes();

            foreach (var siteScope in siteScopes)
            {
                SiteScope scope = new SiteScope();

                scope.ScopeColor = siteScope.color;
                scope.ScopeCount = siteScope.categoryCount;
                scope.ScopeIcon = siteScope.icon;
                scope.ScopeId = siteScope.scopeid;
                scope.ScopeName = siteScope.name;

                scopes.Add(scope);
            }
            return Json(scopes, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult GetScopeListForAPICall()
        {
            List<SiteLocation> scopes = new List<SiteLocation>();
            List<getSiteLocationsForAPICall_Result> siteScopes = HomeModel.GetSitesForAPICall();

            foreach (var siteScope in siteScopes)
            {
                SiteLocation scopeLocation = new SiteLocation();

                scopeLocation.SiteId = siteScope.siteid;
                scopeLocation.SiteAddress1 = siteScope.address1;
                scopeLocation.SiteAddress2 = siteScope.address2;
                scopeLocation.SiteAddress3 = siteScope.address3;
                scopeLocation.SiteCity = siteScope.city;
                scopeLocation.SiteCountry = siteScope.region;
                scopeLocation.SitePostcode = siteScope.postcode;

                scopes.Add(scopeLocation);
            }
            return Json(scopes, JsonRequestBehavior.AllowGet);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
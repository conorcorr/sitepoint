﻿namespace MIA.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    public class SiteScope
    {
        /// <summary>
        /// contains the color associated with the scope
        /// </summary>
        public string ScopeColor { get; set; }

        /// <summary>
        /// contains the count of the sites associated with the scope
        /// </summary>
        public int? ScopeCount { get; set; }

        /// <summary>
        /// contains the icon associated with the scope
        /// </summary>
        public string ScopeIcon { get; set; }

        /// <summary>
        /// contains the scope id
        /// </summary>
        public int ScopeId { get; set; }

        /// <summary>
        /// contains the scope name
        /// </summary>
        public string ScopeName { get; set; }


    }
}
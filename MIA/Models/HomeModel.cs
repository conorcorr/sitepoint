﻿namespace MIA.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    public static class HomeModel
    {
        /// <summary>
        /// gets a list of all sites that have latitude and longitude values set
        /// </summary>
        /// <returns></returns>
        public static List<getSiteLocations_Result> GetSites()
        {
            using(var db = new MIAEntities())
            {
                return db.getSiteLocations().ToList();
            }
        }

        /// <summary>
        /// gets a list of all site scopes which have associated sites that have latitude and longitude values set
        /// </summary>
        /// <returns></returns>
        public static List<getSiteLocationScope_Result> GetSiteScopes()
        {
            using (var db = new MIAEntities())
            {
                return db.getSiteLocationScope().ToList();
            }
        }

        /// <summary>
        /// gets all the sites in the database with their addresses for google maps api call to get lat/long
        /// </summary>
        /// <returns></returns>
        public static List<getSiteLocationsForAPICall_Result> GetSitesForAPICall()
        {
            using (var db = new MIAEntities())
            {
                return db.getSiteLocationsForAPICall().ToList();
            }
        }

    }
}
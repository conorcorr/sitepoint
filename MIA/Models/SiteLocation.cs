﻿namespace MIA.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    /// <summary>
    /// contains the site location values
    /// </summary>
    public class SiteLocation
    {
        /// <summary>
        /// contains the latitude coordinate
        /// </summary>
        public string Latitude { get; set; }

        /// <summary>
        /// contains the longitude coordinate
        /// </summary>
        public string Longitude { get; set; }

        /// <summary>
        /// contains the color associated with the scope
        /// </summary>
        public string ScopeColor { get; set; }

        /// <summary>
        /// contains the icon associated with the scope
        /// </summary>
        public string ScopeIcon { get; set; }

        /// <summary>
        /// contains line one of the site address
        /// </summary>
        public string SiteAddress1 { get; set; }

        /// <summary>
        /// contains line two of the site address
        /// </summary>
        public string SiteAddress2 { get; set; }

        /// <summary>
        /// contains line three of the site address
        /// </summary>
        public string SiteAddress3 { get; set; }

        /// <summary>
        /// contains the city name of the site address
        /// </summary>
        public string SiteCity { get; set; }

        /// <summary>
        /// contains the country name of the site address
        /// </summary>
        public string SiteCountry { get; set; }

        /// <summary>
        /// conatins the site id
        /// </summary>
        public int SiteId { get; set; }

        /// <summary>
        /// contains the site name
        /// </summary>
        public string SiteName { get; set; }

        /// <summary>
        /// contains the postcode of the site address
        /// </summary>
        public string SitePostcode { get; set; }

        /// <summary>
        /// contains the site scope details
        /// </summary>
        public SiteScope SiteScope { get; set; }
    }
}